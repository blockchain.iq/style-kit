


## Basic Grid Setup


### Container
```
.container {
    margin: 0 auto;
    max-width: 112rem;
    padding: 0 2rem;
    position: relative;
    width: 100%;
}
```
### Row

```
.row {
    display: flex;
    flex-direction: column;
    padding: 0;
    width: 100%;
}
```

var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'lander'
    },
    port: process.env.PORT || 3000,
    db: {db: 'lander_development'}
  },

  test: {
    root: rootPath,
    app: {
      name: 'lander'
    },
    port: process.env.PORT || 3000,
    db: {db: 'lander_test'}
  },

  production: {
    root: rootPath,
    app: {
      name: 'lander'
    },
    port: process.env.PORT || 3000,
    db: {db: 'lander_production'}
  }
};

module.exports = config[env];
